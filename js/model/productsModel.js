export class CartItem {
  constructor(product, quantity) {
    this.quantity = quantity;
    this.product = {
      id: product.id,
      name: product.name,
      price: product.price,
      img: product.img,
    };
    this.totalPrice = () => {
      return this.product.price * this.quantity;
    };
  }
}
