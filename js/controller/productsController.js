// render danh sách sản phẩm
export let renderList = (list) => {
  let contentHTML = "";

  list.forEach((product) => {
    let content = `
    <div class="card__item">
            <div class="card__img">
              <img
                src= ${product.img}
              />
            </div>
            <div class="card__body">
              <h2>${product.name}</h2>
              <h3>Thiết kế mang tính đột phá</h3>
              <div class="card__info">
                <ul>
                  <li>
                    Type:
                    <span>${product.type}</span>
                  </li>
                  <li>
                    Screen:
                    <span>${product.screen}</span>
                  </li>
                  <li>
                    Back Camera:
                    <span>${product.backCamera}</span>
                  </li>
                  <li>
                    Front Camera:
                    <span>${product.frontCamera}</span>
                  </li>
                </ul>
              </div>
              <div class="card__footer">
                <p>$${product.price}</p>
                <button onclick="addCart('${product.id}')" class="btn__item">
                  <i class="fa fa-shopping-cart"></i>
                  <span>Thêm vào giỏ</span>
                </button>
              </div>
            </div>
          </div>
    
    `;
    contentHTML += content;
  });

  document.getElementById("list__smartPhone").innerHTML = contentHTML;
};

// Tìm kiếm vị trí product
export let productLocation = (arr, id) => {
  return arr.findIndex((item) => {
    return item.id == id;
  });
};

// tìm kiếm vị trí của giỏ hàng
export let cartLocation = (arr, id) => {
  return arr.findIndex((item) => {
    return item.product.id == id;
  });
};

//render sản phẩm đc chọn vào giỏ hàng
export let renderCart = (list) => {
  let contentHTML = "";
  let totalPrices = 0;
  let totalQuantity = 0;
  list.forEach((item) => {
    let content = `
    <div class="cart__item">
            <div class="cart__img">
              <img
                src="${item.product.img}"
              />
            </div>
            <p class="name">${item.product.name}</p>
            <div class="qty-change">
              <button onclick="changeDown('${
                item.product.id
              }')" class="btn-qty">
                <i class="fas fa-chevron-left"></i>
              </button>
              <p class="qty">${item.quantity}</p>
              <button onclick="changeUp('${item.product.id}')" class="btn-qty">
                <i class="fas fa-chevron-right"></i>
              </button>
            </div>
            <p class="price">$${item.totalPrice()}</p>
            <button onclick="removeCartItem('${
              item.product.id
            }')" class="remove__item">
              <i class="fas fa-trash"></i>
            </button>
          </div>
    `;
    contentHTML += content;
    totalQuantity += item.quantity;
    totalPrices += item.totalPrice();
  });

  if (list.length == 0) {
    document.getElementById("cartBody").innerHTML = `<p class="cart__title">
    Chưa có sản phẩm trong giỏ hàng
    </p>`;
  } else {
    document.getElementById("cartBody").innerHTML = contentHTML;
  }

  document.querySelector(".total").innerHTML = totalPrices;
  document.querySelector(".total-qty").innerHTML = totalQuantity;
};
